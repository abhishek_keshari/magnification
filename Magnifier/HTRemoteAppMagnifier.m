//
//  HTRemoteAppMagnifier.m
//  hopTo
//
//  Created by sujay sundaram on 5/18/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//


#import "HTRemoteAppMagnifier.h"
#import <QuartzCore/QuartzCore.h>

@implementation HTRemoteAppMagnifier

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.layer.cornerRadius = frame.size.width/2;
        self.layer.masksToBounds = YES;
        self.layer.opacity = 0.1;
        UITapGestureRecognizer *magnifierTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGesture:)];
        magnifierTap.numberOfTapsRequired = 1;
        [self addGestureRecognizer:magnifierTap];
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    //	CGContextTranslateCTM(context,1*(self.bounds.size.width),-1*(self.bounds.size.height));
    CGContextScaleCTM(context, 1, 1);
    CGContextTranslateCTM(context,-1*(_x-self.bounds.size.width/2),-1*(_y - self.bounds.size.height/2));
    [self.viewToMagnify.layer renderInContext:context];
    CGPoint centerPoint = CGPointMake(_x,_y );
    self.center = centerPoint;
}

-(void) dismissMagnifier:(onAnimationCompletion) completion
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(0.1, 0.1);
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                         if (completion) {
                             completion(self);
                         }
                     }];
    
}

-(void) handleTapGesture:(UITapGestureRecognizer *) gesture
{
    NSLog(@"Magnifier tapped");
}

-(void) handlePanGesture:(UIScreenEdgePanGestureRecognizer *) recognizer
{
    NSLog(@"Magnifier pan");
    
}

-(void)touchesStarted:(NSSet *)touches withEvent:(UIEvent *)event{
     NSLog(@"touchesStarted pan");
    self.layer.opacity = 0.1;
}

@end
