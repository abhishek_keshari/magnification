//
//  DummyMagnifier.m
//  Magnifier
//
//  Created by Abhishek Raj Keshari on 07/06/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//

#import "MagnifierEdge.h"

@implementation MagnifierEdge


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.


- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        
        [self.layer setBorderColor:[UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1].CGColor];
        self.layer.borderWidth = 5;
        self.layer.cornerRadius = frame.size.width/2;
        self.layer.masksToBounds = YES;
        self.layer.opacity = 0.9;

    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(context, 1, 1);
    CGContextTranslateCTM(context,-1*(_x-self.bounds.size.width/2),-1*(_y - self.bounds.size.height/2));
    [self.viewToMagnify.layer renderInContext:context];
    
}


-(void) dismissMagnifier:(onAnimationCompletion) completion{
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.transform = CGAffineTransformMakeScale(0.1, 0.1);
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                         if (completion) {
                             completion(self);
                         }
                     }];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.magnifierDelegate touchesStarted:touches withEvent:event];
    self.layer.opacity = 0.9;
}

@end
