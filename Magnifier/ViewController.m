//
//  ViewController.m
//  Magnifier
//
//  Created by sujay sundaram on 5/18/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//

#import "ViewController.h"
#import "HTRemoteAppMagnifier.h"
#import "MagnifierEdge.h"

@interface ViewController ()
{
    UIImageView *imageView;
    CGFloat lastScale;
    CGPoint lastPoint;
    UIPanGestureRecognizer *panGesture ;
}

@property (nonatomic,strong) HTRemoteAppMagnifier *magnifier;
@property (nonatomic,strong) MagnifierEdge *dMagnifier;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchGesture:)];
    [self.view addGestureRecognizer:pinch];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, self.view.bounds.size.width, self.view.bounds.size.height)];
    imageView.image = [UIImage imageNamed:@"test.jpg"];
    [self.view addSubview:imageView];
}


-(void) viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    imageView.frame =CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Magnifier

-(void) magnifierProperties
{
    [self.view insertSubview:self.magnifier aboveSubview:imageView];
    [self.view insertSubview:self.dMagnifier aboveSubview:imageView];
    [self setViewToMagnify:imageView];
}

-(HTRemoteAppMagnifier *) magnifier
{
    if (!_magnifier) {
        _magnifier = [[HTRemoteAppMagnifier alloc] initWithFrame:CGRectMake(10, 10, 80, 80)];
        [self magnifierProperties];
    }
    return _magnifier;
}

-(MagnifierEdge *) dMagnifier{
    
    if (!_dMagnifier) {
        _dMagnifier = [[MagnifierEdge alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
        panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMagnifierPanGesture:)];
        panGesture.minimumNumberOfTouches = 1;
        panGesture.maximumNumberOfTouches = 1;
        _dMagnifier.magnifierDelegate = _magnifier.self;
        [_dMagnifier addGestureRecognizer:panGesture];
    }
    return _dMagnifier;
}

-(void) setViewToMagnify:(UIView *)viewToMagnify
{
    self.magnifier.viewToMagnify = viewToMagnify;
    self.dMagnifier.viewToMagnify = viewToMagnify;
}

- (void)handlePinchGesture:(UIPinchGestureRecognizer *)recognizer {
    
    if ([recognizer numberOfTouches] < 2)
        return;
    
    //View on which this gesture is recognized
    UIView *sendersView = recognizer.view;
    CGPoint firstPoint = [recognizer locationOfTouch:0 inView:sendersView];
    CGPoint secondPoint = [recognizer locationOfTouch:1 inView:sendersView];
    CGFloat maxX = MAX(firstPoint.x, secondPoint.x);
    CGFloat maxY = MAX(firstPoint.y, secondPoint.y);
    CGFloat x =  MIN(firstPoint.x, secondPoint.x) + (maxX - MIN(firstPoint.x, secondPoint.x))/2;
    CGFloat y =  MIN(firstPoint.y, secondPoint.y) + (maxY - MIN(firstPoint.y, secondPoint.y))/2;
    
    NSLog(@"In handlePinchGesture: %f, %f, %f, %f", firstPoint.x, firstPoint.y, secondPoint.x, secondPoint.y);
    NSLog(@"In handlePinchGesture: x=%f, y=%f, maxx=%f, maxy=%f", x, y, maxX, maxY);
    
    [self.magnifier.layer setAffineTransform: CGAffineTransformScale([self.view.layer affineTransform],recognizer.scale,recognizer.scale)];
    
    [self.dMagnifier.layer setAffineTransform: CGAffineTransformScale([self.view.layer affineTransform],recognizer.scale,recognizer.scale)];
    self.magnifier.center = CGPointMake(x, y);
    self.dMagnifier.center = CGPointMake(x, y);
    self.magnifier.x = x;
    self.magnifier.y = y;
    self.dMagnifier.x = x;
    self.dMagnifier.y = y;
    NSLog(@"x.....%f,y....%f",self.magnifier.center.y,self.magnifier.center.x);
    [self.dMagnifier setNeedsDisplay];
    [self.magnifier setNeedsDisplay];
    _dMagnifier.layer.opacity = 1;
    _magnifier.layer.opacity = 1;
    
   }


- (void) handleMagnifierPanGesture:(UIPanGestureRecognizer *)recognizer{
    
    CGPoint translation = [recognizer translationInView:self.view];
    CGPoint center = recognizer.view.center;
    CGPoint touchPoint = [recognizer locationInView:_dMagnifier];
    touchPoint = CGPointMake(touchPoint.x * recognizer.view.contentScaleFactor, touchPoint.y *recognizer.view.contentScaleFactor);
    NSLog(@"Center = %@ & touch Point = %@",NSStringFromCGPoint(center),NSStringFromCGPoint(touchPoint));
    
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    self.magnifier.x = self.dMagnifier.x = recognizer.view.center.x;
    self.magnifier.y = self.dMagnifier.y = recognizer.view.center.y;
    [self.dMagnifier setNeedsDisplay];
    [self.magnifier setNeedsDisplay];
    if(recognizer.state == UIGestureRecognizerStateEnded)
    {
        //All fingers are lifted.
        _dMagnifier.layer.opacity = 1;
        _magnifier.layer.opacity = 1;
    }else if (recognizer.state == UIGestureRecognizerStateBegan){
        _dMagnifier.layer.opacity = 0.9;
        _magnifier.layer.opacity = 0.1;
    }else if (recognizer.state == UIGestureRecognizerStateChanged){
        _dMagnifier.layer.opacity = 0.9;
        _magnifier.layer.opacity = 0.1;
    }
}

-(void) handleSingleTap:(id) gesture
{
    if ([_magnifier superview]) {
        [self.magnifier dismissMagnifier:^(UIView *view){
            [view removeFromSuperview];
            _magnifier = nil;
        }];
    }
    if ([_dMagnifier superview]) {
        [self.dMagnifier dismissMagnifier:^(UIView *view){
            [view removeFromSuperview];
            _dMagnifier = nil;
        }];
    }
}


@end
