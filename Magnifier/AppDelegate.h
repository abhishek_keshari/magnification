//
//  AppDelegate.h
//  Magnifier
//
//  Created by sujay sundaram on 5/18/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

