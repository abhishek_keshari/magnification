//
//  DummyMagnifier.h
//  Magnifier
//
//  Created by Abhishek Raj Keshari on 07/06/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^onAnimationCompletion) (UIView *view);
@protocol MagnifierEdgeDelegate <NSObject>

-(void)touchesStarted:(NSSet *)touches withEvent:(UIEvent *)event;

@end
@interface MagnifierEdge : UIView<UIScrollViewAccessibilityDelegate>

/* This class doesnt work well if you add it as a subview of the view that is needed to be magnified
 * for ex: Lets say class A needs to be magnified, then add this view as a sub view of the parent of class A
 */
@property (nonatomic) UIView *viewToMagnify;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;
@property (nonatomic,weak) id<MagnifierEdgeDelegate> magnifierDelegate;

-(void) dismissMagnifier:(onAnimationCompletion) completion;
@end
