//
//  HTRemoteAppMagnifier.h
//  hopTo
//
//  Created by sujay sundaram on 5/18/15.
//  Copyright (c) 2015 hopTo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MagnifierEdge.h"

typedef void (^onAnimationCompletion) (UIView *view);

@interface HTRemoteAppMagnifier : UIView<UIScrollViewAccessibilityDelegate,MagnifierEdgeDelegate>

/* This class doesnt work well if you add it as a subview of the view that is needed to be magnified 
 * for ex: Lets say class A needs to be magnified, then add this view as a sub view of the parent of class A
 */
@property (nonatomic) UIView *viewToMagnify;
@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;

-(void) dismissMagnifier:(onAnimationCompletion) completion;
- (void)drawRect:(CGRect)rect;
@end
